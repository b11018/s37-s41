const express = require('express');
const router = express.Router();

// import from courseControllers
const courseControllers = require('../controllers/courseControllers');
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// route for course creation
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

// route for get all course docs
router.get('/', courseControllers.getAllCourses)

// ACTIVITY 3
// getSingleCourse
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);

// updating a course
router.put('/updateCourse/:id', verify, verifyAdmin, courseControllers.updateCourse);

// ACTIVITY 4
// archiving a course
router.put('/archive/:id', verify, verifyAdmin, courseControllers.archiveCourse);

// activating a course
router.put('/activate/:id', verify, verifyAdmin, courseControllers.activateCourse);

// get active courses
router.get('/getActiveCourses', courseControllers.getActiveCourses);

// find courses by name
router.post('/findCoursesByName', courseControllers.findCoursesByName);

module.exports = router;