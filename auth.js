// [SECTION] DEPENDENCIES
const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	console.log(user);
	// data object is created to contain some details of user
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	console.log(data);
	return jwt.sign(data, secret, {});
};

// NOTES:
/*
	1. You can only get access token when a user logs in your app with the correct credentials
	2. As a user, you can only get your own details from your own token from logging in
	3. JWT is not meant for sensitive data.
	4. JWT is like a passport, you use around the app to access certain features meant for your type of user
*/

// goal: to check if the token exists or it is not tampered
module.exports.verify = (req, res, next) => {
	// req.headers.authorization - contains jwt/token
	let token = req.headers.authorization;
	// result of 'typeof' is string. ergo, compared to a string.
	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token"})
	} else {
		/*
			slice method - used on arrays and strings
			Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWIxNDM0OGEwMmVhMDQ0YjUxZSIsImVtYWlsIjoiaG92ZXJCb2FyZDE5ODVAZ21haWwuY29tIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTY1NDEzMTkxOX0.TzWCMYAI9abiA1QFmrAGA7eaknGihuQ9KbbajxWchRo
		*/
		token = token.slice(7, token.length)
		console.log(token);
		jwt.verify(token, secret, (err, decodedToken) => {
			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else {
				console.log(decodedToken);
				req.user = decodedToken
				// next() will let us proceed to the next middleware or controller
				next();
			}
		})
	}
};

// verifying an admin
module.exports.verifyAdmin = (req, res, next) => {
	if(req.user.isAdmin){
		next();
	} else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
};