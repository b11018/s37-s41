/*
Activity 1:

    >> Create a User model out of the rough sketch made for our models.
    >> Follow the sketch and add the fields and the types.
        Note: All fields are required except for isAdmin.
        Note: isAdmin, 
              Date, 
              status = "Enrolled" 
              has a default value.
*/
const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course Id is required"]
			},
			status: {
				type: String,
				default: "Enrolled"
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			}
		}
	]
});
module.exports = mongoose.model("User", userSchema);