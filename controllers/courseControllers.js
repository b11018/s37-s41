// import course model
const Course = require('../models/Course');

// controller for course creation
module.exports.addCourse = (req, res) => {
	console.log(req.body);
	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});
	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err));
};

// controller for get all course docs
module.exports.getAllCourses = (req, res) => {
	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// ACTIVITY 3
// getSingleCourse
module.exports.getSingleCourse = (req, res) => {
	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// [SECTION] UPDATING A COURSE
module.exports.updateCourse = (req, res) => {
	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}
	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};

// ACTIVITY 4
// archiving a course
module.exports.archiveCourse = (req, res) => {
	let updates = {
		isActive: false
	};
	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(archivedCourse => res.send(archivedCourse))
	.catch(err => res.send(err));
};

// activate a course
module.exports.activateCourse = (req, res) => {
	let updates = {
		isActive: true
	};
	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(activatedCourse => res.send(activatedCourse))
	.catch(err => res.send(err));
};

// get active courses
module.exports.getActiveCourses = (req, res) => {
	Course.find({isActive: true})
	.then(activatedCourse => res.send(activatedCourse))
	.catch(err => res.send(err));
};

// [SECTION] FIND COURSES BY NAME
module.exports.findCoursesByName = (req, res) => {
	console.log(req.body); // contains the name of the course you are looking for
	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {
		if(result.length === 0){
			return res.send('No courses found');
		} else {
			return res.send(result);
		}
	})
}