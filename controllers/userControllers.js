// [SECTION] DEPENDENCIES
const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcryptjs');
const auth = require('../auth')

// [SECTION] REGISTER USER
module.exports.registerUser = (req, res) => {
	console.log(req.body);
	/*
		bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)
		salt rounds - number of times the characters in the hash are randomized
	*/
	const hashedPW = bcrypt.hashSync(req.body.password, 10);
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	});
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
};

// [SECTION] RETRIEVAL OF ALL USERS
module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// [SECTION] LOGIN USER
module.exports.loginUser = (req, res) => {
	console.log(req.body);
	/*
		1. find the user by the email
		2. If we find the user, we will check the password
		3. If we don't find the user, then we will send a message to the client
		4. Upon checking, if the found user's password is the same as our input password, we will generate the 'token/key' to access our app. If not, we will turn them away by sending a message to the client
	*/
	User.findOne({email: req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send("No user found in the database");
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			console.log(isPasswordCorrect);
			/*
				compareSync()
				will return a boolean value
				so if it matches, this will return true
				if not, this will return false
			*/
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send("Incorrect password, please try again");
			}
		} 
	})
	.catch(err => res.send(err));
};

// [SECTION] GETTING SINGLE USER DETAILS
module.exports.getUserDetails = (req, res) => {
	console.log(req.user);
	/*
		{
			expected output:
			id: zxcvasdfqwer1234
			email: test@mail.com
			isAdmin: false
		}
	*/
	// find the logged in user's document from our db and send it to the client by its id
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

/* tried activating through postman. failed.
module.exports.updateUserIsadmin = (req, res) => {
	activateIsAdmin = (req.params.id).isAdmin = true;
	User.findByIdAndUpdate(req.params.id, activateIsAdmin, {new: true})
	.then(updatedIsAdmin => res.send(updatedIsAdmin))
	.catch(err => res.send(err));
}
*/

// ACTIVITY 3
// checkEmailExists
module.exports.checkEmailExists = (req, res) => {
	User.findOne({email: req.body.email})
	.then(foundEmail => {
		if(foundEmail === null){
			return res.send("Email is available")
		} else {
			return res.send("Email is already registered!")
		}
	})
	.catch(err => res.send(err));
};

// 	[SECTION] UPDATING USER DETAILS
module.exports.updateUserDetails = (req, res) => {
	console.log(req.body); // input for new values
	console.log(req.user.id); // check the logged in user's id
	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	};
	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

// [SECTION] UPDATE AN ADMIN
module.exports.updateAdmin = (req, res) => {
	console.log(req.user.id); // id of the logged in user
	console.log(req.params.id); // id of the user we want to update
	let updates = {
		isAdmin: true
	};
	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

// [SECTION] ENROLLMENT
/* async
	- makes the function asynchronous
	- used here because two models are being handled
*/
module.exports.enroll = async (req, res) => {
	/*
		Steps:
		1. Look for the user by its id
			-- push the details of the course we're trying to enroll in. We'll push to a new subdocument in our user.
		2. Look for the course by its id
			-- push the details of the user/enrollee. We'll push to a new enrollees subdocument in our course.
		3. When both saving of documents are successful, we send a message to the client.
	*/
	console.log(req.user.id); // the user's id from the decoded token after verify
	console.log(req.body.courseId); // the course from our request body

	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	};

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		console.log(user);
		/*
			{
				id: 48g4sa54ds2d1s,
				firstName: <userValue>,
				lastName: <userValue>,
				email: <userValue>,
				password: <userValue>,
				mobileNo: <userValue>,
				isAdmin: <userValue>,
				enrollments: [{<userValue>}]
			}
		*/
		let newEnrollment = {
			courseId: req.body.courseId
		}
		user.enrollments.push(newEnrollment);
		return user.save()
		.then(user => true)
		.catch(err => err.message);
	});
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	};
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		console.log(course);
		/*
			id: 48g4sa54ds2d1s,
			name- string,
			description- string,
			price- number,
			isActive- boolean,
					default: true
			createdOn: date,
			enrollees: [
				{
					userId: string,
					status: string,
					dateEnrolled: date
				}
			]
		*/
		let enrollee = {
			userId: req.user.id
		};
		course.enrollees.push(enrollee)
		return course.save()
		.then(course => true)
		.catch(err => err.message);
	});
	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	};
	if(isUserUpdated && isCourseUpdated){
		return res.send({message: 'User enrolled successfully'})
	};
};

// [SECTION] GET ENROLLMENTS
module.exports.getEnrollments = (req, res) => {
	User.findById(req.user.id)
	.then(result => res.send(result.enrollments))
	.catch(err => res.send(err));
};